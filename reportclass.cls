\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{reportclass}[My Thesis Template]

\DeclareUnicodeCharacter{2212}{\ensuremath{-}}

\PassOptionsToClass{12pt}{memoir}
\PassOptionsToClass{a4paper}{memoir}
\LoadClass{memoir}

%%%%
% Packages
%%%%
\RequirePackage{config/requirepackages}
\RequirePackage{config/configuration}
\RequirePackage{config/commands}
\RequirePackage{config/title_page}
\RequirePackage{config/mol2chemfig}

%

\endinput
